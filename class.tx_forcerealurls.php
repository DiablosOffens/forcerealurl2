<?php

class tx_forcerealurls {

    /**
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     */
	function getQueryParamsForLinkData($pObj) {
		$origParams = \TYPO3\CMS\Core\Utility\GeneralUtility::explodeUrl2Array(\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('QUERY_STRING'));

		// Get linkVars from config, because calculateLinkVars removes vars if they are invalid in current query string,
		// so to exclude all linkVars from the new query string, we need the original configuration value.
		$linkVars = '';
		$linkVarsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', (string)$pObj->config['config']['linkVars']);
        if (!empty($linkVarsConfig)) {
			foreach ($linkVarsConfig as $linkVar) {
				if (preg_match('/^(.*)\\((.+)\\)$/', $linkVar, $match)) {
					$linkVar = trim($match[1]);
				}
				if ($linkVar === '') {
					continue;
				}
				$linkVars .= '&' . $linkVar;
			}
        }

        $exclude = 'id&type&cHash' . $linkVars;
		$exclude = \TYPO3\CMS\Core\Utility\GeneralUtility::explodeUrl2Array($exclude, true);
		// never repeat id
		$exclude['id'] = 0;
		$newQueryArray = \TYPO3\CMS\Core\Utility\ArrayUtility::arrayDiffAssocRecursive($origParams, $exclude);
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($newQueryArray, [], false);
		return \TYPO3\CMS\Core\Utility\GeneralUtility::implodeArrayForUrl('', $newQueryArray, '', false, true);
	}

    /**
     * @param $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     */
	function check($params, $pObj) {
		if ($pObj->siteScript && $pObj->config['config']['tx_realurl_enable'] && (
				substr($pObj->siteScript, 0, 9) == 'index.php' ||
				substr($pObj->siteScript, 0, 1) == '?'
				) 
				&& strpos($pObj->siteScript, '&noforce=1') === false // A way to skip redirect. Thanks goes to Reindl Bernd.
				&& strpos($pObj->siteScript, '&jumpurl=') == false // Skip redirect if URL is for jumpurl. Thanks goes to Reindl Bernd.
                && !$pObj->isBackendUserLoggedIn() // Skip redirect if we are in BE Logged in
			) {
			$baseURL = $pObj->config['config']['baseURL'];
			$LD = $pObj->tmpl->linkData($pObj->page, '', $pObj->no_cache, '', '', $this->getQueryParamsForLinkData($pObj));
			
			if (strtolower($LD['totalURL']) != strtolower($pObj->siteScript)) {
				$url = rtrim($baseURL, '/') . '/' . ltrim($LD['totalURL'], '/');
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $url);
				exit;
			}
		}
	}

}

?>